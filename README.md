# JPK gate

A polish Ministry of Finances JPK WebService gate.

## Usage

### prepareDocumentToSend method

Prepares the metadata file for signature with an electronic signature. The file is created based on the main JPK file. The file is subjected to ZIP compression resulting in one ZIP file containing a single JPK document. If the size of the received ZIP file exceeds 60MB then it is binary divided into the appropriate number of parts with a size of 60MB each and the last part with a size not exceeding 60MB. Then the resulting parts are encrypted with a randomly generated AES key. This key is encrypted with a public key provided by the Ministry of Finance and saved in a metadata file along with information about the original JPK file as well as information about the encrypted parts.

Constructor params:

- **$dir** - a directory to store generated files. The directory must exists before call a library and have to read/write rights.

Input data:

- **$file** - location of XML JPK file.

Output data (array):

- array with a string to sign and the location of the file on the disk
- array with location of the ZIP file and encrypted parts
- unencrypted AES key
- base64 encrypted of encryption vector

```php
<?php

use SJRoyd\MF\EDokumenty\JPK;

$dir = 'some/dierctory'; // directory must exists
$jpkFileLocation = 'file/location'; // location of XML JPK file

$jpk = new JPK($dir);
$initData = $jpk->prepareDocumentToSend($jpkFileLocation);
/* [
    'init' => [
        'xml' => '', // XML metadata string for signing
        'file' => '', // XML metadata file location
    ],
    'jpk' => [
        'zip' => '', // ZIP compressed JPK XML file location
        'chunks' => [], // AES encrypted JPK XML ZIP file chunks
    ],
    'aesKey' => '', // AES key for encrypted ZIP chunks
    'vectorKey' => '' // base64 vector key for AES encryptor
] */
```


### sendDocument method

The method is used to send the signed document and encrypted files through the gate of the Ministry of Finance and receive the document processing status.

Constructor params:

- **$dir** - a directory where are stored previously generates AES encrypted files.

Input data:

- **$file** - signed metadata XML file location.
- **$validateQualSig** - boolean value, if true (in the test environment) is passed, the system will verify that the file being sent has been signed with a valid Polish or European qualified signature or a Trusted Profile.

Output (Response\Status instance) methods:

- **getCode()** - status code
- **getDescription()** - description
- **getMessage()** - return "($code) $description";
- **getDetails()** - event details
- **getTimestamp()** - timestamp
- **getUpo()** - optional, Official Certificate of Receipt (UPO) 

Codes list:
 
- 100 - File transfer session started.
- 101 - X of Y declared files were received.
- 102 - Please resend UPO request.
- 110 - Session has expired, no specified number of files uploaded.
- 120 - The session has ended correctly. The data has been saved correctly. The document is being verified.
- 200 - Document processing completed successfully, download the UPO.
- 300 - Invalid reference number.
- 301 - Document being processed, check the result of the next document verification.
- 302 - Pre-processed document, check the result of the next document verification.
- 303 - Document is being verified, check the result of the next document verification.
- 401 - Negative verification - document not in line with XSD schema.
- 403 - Document with incorrect signature.
- 404 - Document with an invalid certificate.
- 405 - Document with revoked certificate.
- 406 - Certificate document with unsupported provider.
- 407 - You have sent a duplicate document. The reference number of the original is XXXXXXXX
- 408 - The document contains errors that prevent its processing.
- 409 - The document contains the wrong number and / or type of elements.
- 410 - Uploaded files are not a valid ZIP archive.
- 411 - Error while merging document (document incorrectly split).
- 412 - Document incorrectly encrypted.
- 413 - Document control sum does not match the declared value.
- 414 - Checksum of the part of the document (file .......) does not match the declared value.
- 415 - The transmitted document type is not supported by the system.

Possible errors (instances of PHP included Exception):

- InitUpload\Error_400

    Error codes:
    
    - 100 - Invalid XML; The specified document is not an XML document
    - 110 - Unsigned document; The document provided is unsigned according to the specifications
    - 111 - The signature is in a format other than XAdES-BES
    - 112 - Incorrectly signed signature. Impossible verification; An unexpected error occurred while verifying the signature
    - 113 - Signature made in unsupported external format (detached); Supported signature formats are enveloped and enveloping
    - 114 - Problem reading the signed object
    - 120 - Signature negatively verified; The signature could not be verified correctly
    - 130 - The references in the signature have been negatively verified. The data has probably been modified
    - 135 - Document with non-qualified signature; The authenticity of the qualified signature is checked in the production environment.
    - 140 - The uploaded file does not comply with the XSD schema; The document could not be verified according to the InitUpload.xsd schema
    - 150 - Unsupported form code: "specific systemCode"; The form code is not supported
    - 160 - The "specific HashValue" value is not encoded in Base64; The abbreviation of the files declared for transfer must be encoded in Base64.
    - 170 - A duplicate of a processed document was sent. Original reference number: XXXXXXXX; Duplicates are checked based on the SHA-256 hash value of the declared JPK document
    
    
- InitUpload\Error_500
- FinishUpload\Error_400
- FinishUpload\Error_500
- Status\Error_400
- Status\Error_500

Methods for errors:

- **getCode()** - error code
- **getMessage()** - error message
- **getRequestId()** - unique id of bad request
- **getErrors()** - optional, text errors list
- **hasErrors()** - checks if exists any item in errors list
- **getFullMessage()** return "($code) $message"

```php
<?php

use SJRoyd\MF\EDokumenty\JPK;
use SJRoyd\MF\EDokumenty\Storage\Response\InitUpload;
use SJRoyd\MF\EDokumenty\Storage\Response\FinishUpload;
use SJRoyd\MF\EDokumenty\Storage\Response\Status;

$dir = 'some/directory'; // a directory with AES encrypted files 
$signedFileLocation = 'file/location'; // a location of signed metadata file

$jpk = new JPK($dir);
try {
    $status = $jpk->sendDocument($signedFileLocation);
    $referenceNumber = $jpk->getReferenceNumber();
} catch (InitUpload\Error_400 $e) {
    echo $e->getFullMessage();
} catch (InitUpload\Error_500 $e) {
    // ...
} catch (FinishUpload\Error_400 $e) {
    // ...
} catch (FinishUpload\Error_500 $e) {
    // ...
} catch (Status\Error_400 $e) {
    // ...
} catch (Status\Error_500 $e) {
    // ...
} catch (\Exception $e){
    echo $e->getMessage();
}
```
### getReferenceNumber method

Method called directly after the sendDocument() method. It allows you to get the reference number of the document you sent.

### checkStatus method

The method returns Official Confirmation of Receipt (UPO) of sent documents.

Input data:

- **$referenceNumber** - reference number.

Output (Response\Status instance) methods: described above.

```php
<?php

use SJRoyd\MF\EDokumenty\JPK;
use SJRoyd\MF\EDokumenty\Storage\Response;

$referenceNumber = '04beb97601e10f210000004546830'; // reference number obtained using the getReferenceNumber() method

$jpk = new JPK();
try {
    $status = $jpk->checkStatus($referenceNumber);
    print_r($status);
} catch (Status\Error_400 $e) {
    echo $e->getFullMessage();
} catch (Status\Error_500 $e) {
    // ...
} catch (\Exception $e){
    echo $e->getMessage();
}
```
