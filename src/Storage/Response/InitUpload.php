<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response;

class InitUpload
{
    /**
     * @var string
     */
    private $referenceNumber;

    /**
     * @var int
     */
    private $timeout;

    /**
     * @var InitUpload\File[]
     */
    private $uploadFilesList;

    /**
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param   string  $refNumber
     *
     * @return InitUpload
     */
    public function setReferenceNumber($refNumber)
    {
        $this->referenceNumber = $refNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param   int  $timeout
     *
     * @return InitUpload
     */
    public function setTimeoutInSec($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @return InitUpload\File[]
     */
    public function getUploadFilesList()
    {
        return $this->uploadFilesList;
    }

    /**
     * @param   InitUpload\File[]  $list
     *
     * @return InitUpload
     */
    public function setRequestToUploadFileList($list)
    {
        $this->uploadFilesList = $list;

        return $this;
    }
}

/*
 * 100 - Niepoprawny XML
 * Podany dokument nie jest dokumentem XML
 * 110 - Nie podpisany dokument
 * Podany dokument jest niepodpisany zgodnie ze specyfikacją
 * 111 - Podpis jest złożony w innym formacie niż XAdES-BES
 * 112 - Niepoprawnie złożony podpis. Niemożliwa weryfikacja
 * W trakcie weryfikacji podpisu wystąpił nieoczekiwany błąd
 * 113 - Podpis złożony w nieobsługiwanym formacie zewnętrznym (detached)
 * Obsługiwane formaty podpisu to enveloped i enveloping
 * 114 - Problem z odczytaniem podpisanego obiektu
 * 120 - Podpis negatywnie zweryfikowany
 * Nie udało się poprawnie zweryfikować podpisu
 * 130 - Referencje w podpisie zostały negatywnie zweryfikowane. Dane prawdopodobnie zostały zmodyfikowane
 * 135 - Dokument z podpisem niekwalifikowanym
 * Na środowisku produkcyjnym sprawdzana jest autentyczność podpisu kwalifikowanego.
 * 140 - Przesłany plik jest niezgody ze schematem XSD
 * Nie udało się zweryfikować dokumentu zgodnie ze schematem InitUpload.xsd
 * 150 - Nieobsługiwany kod formularza :  „konkretny systemCode”
 * Kod formularza jest nieobsługiwany
 * 160 - Wartość „konkretny HashValue” nie jest zakodowana w Base64
 * Skrót plików zadeklarowanych do przesłania musi być zakodowany w Base64.
 * 170 - Przesłano duplikat przetworzonego dokumentu. Numer referencyjny oryginału: XXXXXXXX
 * Duplikaty są sprawdzane na podstawie wartości skrótu SHA-256 zadeklarowanego dokumentu JPK
 */