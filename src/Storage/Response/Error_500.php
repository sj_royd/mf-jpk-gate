<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response;

class Error_500 extends Error
{
    protected $code = 500;
}
