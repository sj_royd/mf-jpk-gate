<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response\InitUpload;

class File
{
    /**
     * @var string
     */
    private $blobName;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $method;

    /**
     *
     * @var File_Header[]
     */
    private $headerList;

    /**
     * @return string
     */
    public function getBlobName()
    {
        return $this->blobName;
    }

    /**
     * @param   string  $name
     *
     * @return File
     */
    public function setBlobName($name)
    {
        $this->blobName = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param   string  $fileName
     *
     * @return File
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param   string  $url
     *
     * @return File
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param   string  $method
     *
     * @return File
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return File_Header[]
     */
    public function getHeaderList()
    {
        return $this->headerList;
    }

    /**
     * @param   File_Header[]  $headerList
     *
     * @return File
     */
    public function setHeaderList($headerList)
    {
        $this->headerList = $headerList;

        return $this;
    }


}
