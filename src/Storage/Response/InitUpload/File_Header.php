<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response\InitUpload;

class File_Header
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param   string  $key
     *
     * @return File_Header
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param   string  $value
     *
     * @return File_Header
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
