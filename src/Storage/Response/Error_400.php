<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response;

class Error_400 extends Error
{
    protected $code = 400;
}
