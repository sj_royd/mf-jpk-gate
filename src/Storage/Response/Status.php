<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response;

class Status {
    /**
     * @var string
     */
    public $Code;
    /**
     * @var string
     */
    public $Description;
    /**
     * @var string
     */
    public $Details;
    /**
     * @var string
     */
    public $Upo;
    /**
     * @var \DateTime
     */
    public $Timestamp;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param   string  $code
     *
     * @return Status
     */
    public function setCode($code)
    {
        $this->Code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param   string  $desc
     *
     * @return Status
     */
    public function setDescription($desc)
    {
        $this->Description = $desc;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->Details;
    }

    /**
     * @param   string  $details
     *
     * @return Status
     */
    public function setDetails($details)
    {
        $this->Details = $details;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpo()
    {
        return $this->Upo;
    }

    /**
     * @param   string  $upo
     *
     * @return Status
     */
    public function setUpo($upo)
    {
        $this->Upo = $upo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->Timestamp;
    }

    /**
     * @param   \DateTime  $timestamp
     *
     * @return Status
     */
    public function setTimestamp($timestamp)
    {
        $this->Timestamp = $timestamp;

        return $this;
    }


    public function getMessage(){
        return "({$this->Code}) {$this->Description}";
    }
}

/*
 * 100 - Rozpoczęto sesję przesyłania plików.
 * 101 - Odebrano X z Y zadeklarowanych plików.
 * 102 - Proszę o ponowne przesłanie żądania UPO.
 * 110 - Sesja wygasła, nie przesłano zadeklarowanej liczby plików.
 * 120 - Sesja została poprawnie zakończona. Dane zostały poprawnie zapisane. Trwa weryfikacja dokumentu.
 * 200 - Przetwarzanie dokumentu zakończone poprawnie, pobierz UPO.
 * 300 - Nieprawidłowy numer referencyjny.
 * 301 - Dokument w trakcie przetwarzania, sprawdź wynik następnej weryfikacji dokumentu.
 * 302 - Dokument wstępnie przetworzony, sprawdź wynik następnej weryfikacji dokumentu.
 * 303 - Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu.
 * 401 - Weryfikacja negatywna – dokument niezgodny ze schematem XSD.
 * 403 - Dokument z niepoprawnym podpisem.
 * 404 - Dokument z nieważnym certyfikatem.
 * 405 - Dokument z odwołanym certyfikatem.
 * 406 - Dokument z certyfikatem z nieobsługiwanym dostawcą.
 * 407 - Przesłałeś  duplikat dokumentu.  Numer referencyjny oryginału to XXXXXXXX
 * 408 - Dokument zawiera błędy uniemożliwiające jego przetworzenie.
 * 409 - Dokument zawiera niewłaściwą ilość i/lub rodzaj elementów.
 * 410 - Przesłane pliki nie są prawidłowym archiwum ZIP.
 * 411 - Błąd podczas scalania dokumentu (dokument nieprawidłowo podzielony).
 * 412 - Dokument nieprawidłowo zaszyfrowany.
 * 413 - Suma kontrolna dokumentu niezgodna z deklarowana wartością.
 * 414 - Suma kontrolna części dokumentu (pliku ....... ) niezgodna z deklarowaną wartością.
 * 415 - Przesłany rodzaj dokumentu nie jest obsługiwany w systemie.
 */