<?php

namespace SJRoyd\MF\EDokumenty\Storage\Response;

use Exception;

class Error extends Exception
{
    /**
     * @var string
     */
    protected $requestId;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @param   string  $code
     *
     * @return Error
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param   string  $requestId
     *
     * @return Error
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return !!$this->errors;
    }

    /**
     * @param   array  $errors
     *
     * @return Error
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @param   string  $message
     *
     * @return Error
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullMessage()
    {
        return "({$this->code}) {$this->message}";
    }


}
