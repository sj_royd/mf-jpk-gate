<?php

namespace SJRoyd\MF\EDokumenty\Storage\Builder;

use SJRoyd\MF\EDokumenty\Storage;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\RSA;
use phpseclib\Crypt\Random;

/**
 * @see https://www.podatki.gov.pl/jednolity-plik-kontrolny/pliki-do-pobrania/
 */
class InitUpload
{

    private $aes_crypt = [
        'prod' => __DIR__ . '/../../../resource/AES_crypt.pem',
        'test' => __DIR__ . '/../../../resource/AES_crypt_test.pem'
    ];

    /**
     * @var AES
     */
    private $cipher;

    /**
     * @var string
     */
    private $aesKey;

    /**
     * @var string
     */
    private $encryptedAesKey;

    /**
     * @var string
     */
    private $vector;

    /**
     * @var string
     */
    private $template = 'InitUpload.xml.twig';

    /**
     * @var string
     */
    private $initFile;

    /**
     * @var string
     */
    private $zipFile;

    /**
     * @var array
     */
    private $zipParts = [];

    /**
     * @var array
     */
    private $aesFiles = [];

    /**
     * @var string
     */
    private $initXml;

    /**
     * Generate the init document
     *
     * @param string $filePath XML file server path
     * @param bool   $test
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __construct($filePath, $test = false)
    {
        $this->aes_crypt = realpath($this->aes_crypt[$test ? 'test' : 'prod']);

        $fileName = pathinfo($filePath, PATHINFO_BASENAME);
        $this->compressDocument($filePath, $fileName);
        $this->splitZipDocument();
        $this->createCipher();
        $this->encryptDocument();
        $this->encryptAesKey();

        $this->composeMetaXML($filePath, $fileName);
    }

    /**
     * Initializes the encryptor
     */
    protected function createCipher()
    {
        $this->cipher = new AES(AES::MODE_CBC);
        $this->cipher->setKeyLength(256);

        $this->aesKey = gen_random_string($this->cipher->getKeyLength() >> 3);
        $this->vector = Random::string($this->cipher->getBlockLength() >> 3);

        $this->cipher->setKey($this->aesKey);
        $this->cipher->setIV($this->vector);
    }

    /**
     * Compresses the document
     *
     * @param string $filePath XML file server path
     * @param string $fileName Final file name
     */
    protected function compressDocument($filePath, $fileName)
    {
        $zipFileName = Storage::$storeDir . "{$fileName}.zip";

        $zip = new \ZipArchive();
        $zip->open(
            $zipFileName,
            \ZipArchive::CREATE | \ZipArchive::OVERWRITE
        );
        $zip->addFile($filePath, $fileName);
        $zip->close();

        $this->zipFile = $zipFileName;
    }

    /**
     * Split the compressed document into 60 MB parts
     */
    protected function splitZipDocument()
    {
        $dir       = Storage::$storeDir;
        $chunkSize = 60 * 1024 * 1024;
        if (filesize($this->zipFile) > $chunkSize) {
            exec("split -d -b 60M {$this->zipFile} {$this->zipFile}.");
            $partsList = glob("{$this->zipFile}.*");
            foreach ($partsList as $partFile) {
                $this->zipParts[] = $dir . pathinfo($partFile, PATHINFO_BASENAME);
            }
        } else {
            $this->zipParts[] = $this->zipFile;
        }
    }

    /**
     * Encrypts split files with the AES key
     */
    protected function encryptDocument()
    {
        foreach ($this->zipParts as $zipPart) {
            $aesFileName = "{$zipPart}.aes";
            file_put_contents(
                $aesFileName,
                $this->cipher->encrypt(
                    file_get_contents($zipPart)
                )
            );

            $this->aesFiles[] = $aesFileName;
        }
    }

    /**
     * Encrypts the AES key with the MF public key
     */
    protected function encryptAesKey()
    {
        // http://phpseclib.sourceforge.net/rsa/2.0/examples.html see Options
        define('CRYPT_RSA_PKCS15_COMPAT', true);

        $d = openssl_pkey_get_details(
            openssl_pkey_get_public(
                file_get_contents($this->aes_crypt)
            )
        );

        $rsa = new RSA();
        $rsa->loadKey($d['key'], $d['type']);
        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $this->encryptedAesKey = $rsa->encrypt($this->aesKey);
    }

    /**
     * Creates the XML init document
     *
     * @param string $xmlFile     XML file path
     * @param string $xmlFileName File name
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function composeMetaXML($xmlFile, $xmlFileName)
    {
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        $formCode = $xml->getElementsByTagName('KodFormularza')->item(0);

        switch ($formCode->nodeValue) {
            case 'JPK_VAT':
            case 'JPK_V7M':
            case 'JPK_V7K':
                $docType = 'JPK';
                break;
            default:
                $docType = 'JPKAH';
        }

        $data = [
            'encKey' => base64_encode($this->encryptedAesKey),
            'doc'    => [
                'type'          => $docType,
                'formCode'      => $formCode->nodeValue,
                'systemCode'    => $formCode->getAttribute('kodSystemowy'),
                'schemaVersion' => $formCode->getAttribute('wersjaSchemy'),
            ],
            'file'   => [
                'name'   => $xmlFileName,
                'size'   => filesize($xmlFile),
                'hash'   => base64_encode(
                    hash_file('sha256', $xmlFile, true)
                ),
                'ivHash' => base64_encode($this->vector),
            ],
        ];
        foreach ($this->aesFiles as $file) {
            $data['aesList'][] = [
                'name' => pathinfo($file, PATHINFO_BASENAME),
                'size' => filesize($file),
                'hash' => base64_encode(hash_file('md5', $file, true)),
            ];
        }

        $twig = new \Twig\Environment(
            new \Twig\Loader\FilesystemLoader(
                realpath(__DIR__ . '/../../../resource')
            )
        );

        $this->initXml  = $twig->render($this->template, $data);
        $this->initFile = Storage::$storeDir . pathinfo($xmlFile, PATHINFO_FILENAME) . '.init.xml';
        file_put_contents($this->initFile, $this->initXml);
    }

    /**
     * @return array
     */
    public function getAllData()
    {
        return [
            'init'      => [
                'xml'  => $this->initXml,
                'file' => $this->initFile,
            ],
            'jpk'       => [
                'zip'    => $this->zipFile,
                'chunks' => $this->aesFiles,
            ],
            'aesKey'    => $this->aesKey,
            'vectorKey' => base64_encode($this->vector)
        ];
    }
}
