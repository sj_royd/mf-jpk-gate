<?php


namespace SJRoyd\MF\EDokumenty;


use SJRoyd\MF\EDokumenty\Storage\Response\InitUpload;
use SJRoyd\MF\EDokumenty\Storage\Response\Status;
use SJRoyd\MF\EDokumenty\Storage\Response\FinishUpload;

class Storage extends EDokumenty
{

    protected $ws_name = 'Storage';

    /**
     * @var InitUpload\File[]
     */
    private $azureRequests;

    /**
     * @var string
     */
    private $reference;

    /**
     * The method initiating the client session. Its call is a prerequisite
     * to transfer data using the Azure Put Blob method
     *
     * @param string|resource $document        File resource or path
     * @param bool            $validateQualSig Validate qualified signature
     *
     * @return string Reference number
     * @throws InitUpload\Error_400
     * @throws InitUpload\Error_500
     */
    public final function InitUploadSigned($document, $validateQualSig = false)
    {
        /* @var $response InitUpload|InitUpload\Error_400|InitUpload\Error_500 */
        $response = $this->callPost(
            __FUNCTION__,
            [
                'params' => [
                    'enableValidateQualifiedSignature' => $validateQualSig
                ],
                'body'   => fopen($document, 'r')
            ], [
                200 => InitUpload::class,
                400 => InitUpload\Error_400::class,
                500 => InitUpload\Error_500::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            $this->azureRequests = $response->getUploadFilesList();
            $this->reference     = $response->getReferenceNumber();
            return $this->reference;
        } else {
            throw $response;
        }
    }

    /**
     * The method sends the basic JPK documents. This is a method directly
     * implemented by the Azure Storage Service
     *
     * @return bool
     * @throws \Exception
     */
    public function putBlob()
    {
        if (!self::$storeDir) {
            throw new \Exception('No storage directory defined');
        }
        if (!$this->azureRequests) {
            throw new \Exception('No MS Azure requests');
        }
        foreach ($this->azureRequests as $request) {
            $filePath = self::$storeDir . $request->getFileName();
            $this->azureCall(
                $request->getUrl(),
                $filePath,
                $request->getHeaderList()
            );
        }
        return true;
    }

    /**
     * The method ending the session. Its call is a prerequisite for the correct
     * completion of the procedure for sending documents. The required files are
     * then checked using the name and MD5 of the values declared in InitUploadSigned.
     * Failure to call it means that the session has been aborted.
     *
     * @return bool
     * @throws FinishUpload\Error_400
     * @throws FinishUpload\Error_500
     */
    public final function FinishUpload()
    {
        $blobList = [];
        foreach ($this->azureRequests as $aR) {
            $blobList[] = $aR->getBlobName();
        }

        /* @var $response FinishUpload\Error_400|FinishUpload\Error_500 */
        $response = $this->callPost(
            __FUNCTION__,
            [
                'json' => [
                    'ReferenceNumber'   => $this->reference,
                    'AzureBlobNameList' => $blobList
                ]
            ], [
                400 => FinishUpload\Error_400::class,
                500 => FinishUpload\Error_500::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return true;
        } else {
            throw $response;
        }
    }

    /**
     * The method returns Official Confirmation of Receipt of sent documents.
     *
     * @param string $reference Reference number
     *
     * @return Status
     * @throws Status\Error_400
     * @throws Status\Error_500
     * @throws \Exception
     */
    public final function Status($reference = null)
    {
        !$reference && $reference = $this->reference;
        if (!$reference) {
            throw new \Exception('Please enter a reference number');
        }

        /* @var $response Status|Status\Error_400|Status\Error_500 */
        $response = $this->callGet(
            __FUNCTION__ . '/' . $reference,
            [],
            [
                200 => Status::class,
                400 => Status\Error_400::class,
                500 => Status\Error_500::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response;
        } else {
            throw $response;
        }
    }
}