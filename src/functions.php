<?php

/**
 * Subset of illuminate/support original Str::random()
 * @param   int  $length
 *
 * @return string
 * @throws Exception
 */
function gen_random_string($length = 16)
{
    $string = '';

    while (($len = strlen($string)) < $length) {
        $size = $length - $len;

        $bytes = random_bytes($size);

        $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
    }

    return $string;
}