<?php

namespace SJRoyd\MF\EDokumenty;

use SJRoyd\HTTPService\RestRequest;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Internal\BlobResources;

/**
 * @see https://www.podatki.gov.pl/media/1138/specyfikacja_interfejsow_uslug_jpk_wersja_2_3.pdf
 */
class EDokumenty extends RestRequest
{
    /**
     * @var string
     */
    public static $storeDir;

    protected $ssl_cert = [
        'prod' => __DIR__ . '/../resource/e_dokumenty.mf.gov.pl.pem',
        'test' => __DIR__ . '/../resource/test_e_dokumenty.mf.gov.pl.pem',
    ];

    protected $ws_path = [
        'prod' => 'https://e-dokumenty.mf.gov.pl/api',
        'test' => 'https://test-e-dokumenty.mf.gov.pl/api'
    ];

    public function __construct($test = false)
    {
        $this->ws_path = $this->ws_path  [$test ? 'test' : 'prod'];
//        $this->ssl_cert  = $this->ssl_cert [$test ? 'test' : 'prod'];
        $this->ssl_cert = null;
        parent::__construct($test);
    }

    /**
     * @param string $storeDir
     */
    public static function setStoreDir($storeDir)
    {
        self::$storeDir = realpath($storeDir) . DIRECTORY_SEPARATOR;
    }

    /**
     * Microsoft Azure call
     *
     * @param string $url
     * @param string $filePath
     * @param array  $additionalHeaders
     *
     * @throws \Exception
     */
    protected function azureCall($url, $filePath, $additionalHeaders = [])
    {
        $pattern = '(?<endPoint>(?<protocol>https?):\/\/(?<accountName>[\w]+)\.blob\.core\.windows\.net)\/(?<storeName>[\w]+)\/(?<blobName>[\w\-]+)\?(?<query>.+)';
        preg_match("~{$pattern}~", $url, $m);

        $connData  = [
//            BlobResources::DEFAULT_ENDPOINTS_PROTOCOL_NAME => $m['protocol'],
            BlobResources::BLOB_ENDPOINT_NAME => $m['endPoint'],
            BlobResources::SAS_TOKEN_NAME     => $m['query'],
        ];
        $connArray = array_map(function ($value, $key) {
            return "{$key}={$value}";
        }, array_values($connData), array_keys($connData));

        try {
            // Create blob REST proxy.
            $blobClient = BlobRestProxy::createBlobService(implode(';', $connArray));

            $content = fopen($filePath, "r");

            //Upload blob
            $blobClient->createBlockBlob($m['storeName'], $m['blobName'], $content);
        } catch (ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            throw new \Exception("({$e->getCode()}) {$e->getErrorMessage()}");
        }
    }

}
