<?php

namespace SJRoyd\MF\EDokumenty;

use SJRoyd\MF\EDokumenty\Storage\Response\InitUpload;
use SJRoyd\MF\EDokumenty\Storage\Response\FinishUpload;
use SJRoyd\MF\EDokumenty\Storage\Response\Status;

class JPK
{
    private $test = false;

    /**
     * @var string
     */
    private $refNo;

    /**
     * JPK constructor.
     *
     * @param         $storageDir
     * @param   bool  $test
     */
    public function __construct($storageDir = null, $test = false)
    {
        $storageDir && Storage::setStoreDir($storageDir);
        $this->test = $test;
    }

    /**
     * @param   string  $file
     *
     * @return array
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function prepareDocumentToSend($file)
    {
        $init = new Storage\Builder\InitUpload($file, $this->test);
        return $init->getAllData();
    }

    /**
     * @param         $filePath
     * @param   bool  $validateQualSig
     *
     * @return Status
     * @throws InitUpload\Error_400
     * @throws InitUpload\Error_500
     * @throws FinishUpload\Error_400
     * @throws FinishUpload\Error_500
     * @throws Status\Error_400
     * @throws Status\Error_500
     */
    public function sendDocument($filePath, $validateQualSig = false)
    {
        $gate = new Storage($this->test);
        $this->refNo = $gate->InitUploadSigned($filePath, $validateQualSig);
        $gate->putBlob();
        $gate->FinishUpload();
        return $gate->Status();
    }

    /**
     * @param $refNo
     *
     * @return Status
     * @throws Status\Error_400
     * @throws Status\Error_500
     */
    public function checkStatus($refNo)
    {
        $gate = new Storage($this->test);
        return $gate->Status($refNo);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getReferenceNumber()
    {
        if(!$this->refNo){
            throw new \Exception('No reference number');
        }
        return $this->refNo;
    }
}